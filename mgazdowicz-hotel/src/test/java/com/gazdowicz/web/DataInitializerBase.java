package com.gazdowicz.web;

/**
 * Created by Mariusz.Gazdowicz on 7/18/2018.
 */

import liquibase.Liquibase;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 */
@Component
public class DataInitializerBase {

    private Liquibase liquibase;

    public void setupTest(String mockFile, JdbcTemplate jdbcTemplate) throws LiquibaseException, SQLException {
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        final DatabaseConnection dbConnection = new JdbcConnection(connection);
        liquibase = new Liquibase(mockFile, new ClassLoaderResourceAccessor(), dbConnection);
        liquibase.update("test");
    }

    public void afterTest() throws LiquibaseException {
        liquibase.rollback(1, "test");
    }

}
