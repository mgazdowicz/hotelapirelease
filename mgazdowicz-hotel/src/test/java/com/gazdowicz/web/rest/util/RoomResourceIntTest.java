package com.gazdowicz.web.rest.util;

import com.gazdowicz.HotelApp;
import com.gazdowicz.domain.Hotel;
import com.gazdowicz.domain.Reservation;
import com.gazdowicz.domain.Room;
import com.gazdowicz.repository.HotelRepository;
import com.gazdowicz.repository.ReservationRepository;
import com.gazdowicz.repository.RoomRepository;
import com.gazdowicz.repository.search.HotelSearchRepository;
import com.gazdowicz.repository.search.ReservationSearchRepository;
import com.gazdowicz.repository.search.RoomSearchRepository;
import com.gazdowicz.service.RoomService;
import com.gazdowicz.service.dto.ReservationDTO;
import com.gazdowicz.service.dto.RoomDTO;
import com.gazdowicz.service.mapper.ReservationMapper;
import com.gazdowicz.service.mapper.RoomMapper;
import com.gazdowicz.web.DataInitializerBase;
import com.gazdowicz.web.TestUtil;
import com.gazdowicz.web.rest.RoomResource;
import com.gazdowicz.web.rest.errors.ExceptionTranslator;
import liquibase.exception.LiquibaseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the RoomResource REST controller.
 *
 * @see RoomResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HotelApp.class)
public class RoomResourceIntTest extends DataInitializerBase {

    private static final Long DEFAULT_NUMBER = 99L;

    private static final Double DEFAULT_DAY_PERIOD_PRICE = 100D;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomSearchRepository roomSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restRoomMockMvc;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ReservationSearchRepository reservationSearchRepository;

    @Autowired
    private ReservationRepository reservationRepository;


    @Before
    public void setup() throws LiquibaseException, SQLException {

        super.setupTest("room_data_mock.xml", jdbcTemplate);

        MockitoAnnotations.initMocks(this);
        final RoomResource roomResource = new RoomResource(roomService);
        this.restRoomMockMvc = MockMvcBuilders.standaloneSetup(roomResource)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }


    @After
    public void afterTest() throws LiquibaseException {
        super.afterTest();
    }

    @Test
    public void createRoom() throws Exception {
        int databaseSizeBeforeCreate = roomRepository.findAll().size();


        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setDayPeriodPrice(DEFAULT_DAY_PERIOD_PRICE);
        roomDTO.setHotelId(100L);
        roomDTO.setNumber(DEFAULT_NUMBER);

        // Create the Room
        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isCreated());

        // Validate the Room in the database
        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeCreate + 1);
        Room testRoom = roomList.get(roomList.size() - 1);

        assertThat(testRoom.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testRoom.getDayPeriodPrice()).isEqualTo(DEFAULT_DAY_PERIOD_PRICE);

        // Validate the Room in Elasticsearch
        Room roomEs = roomSearchRepository.findOne(testRoom.getId());
        assertThat(roomMapper.toDto(roomEs)).isEqualToComparingFieldByField(roomMapper.toDto(testRoom));
    }

    @Test
    public void createRoomToNonExistingHotel() throws Exception {

        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setDayPeriodPrice(DEFAULT_DAY_PERIOD_PRICE);
        roomDTO.setHotelId(Long.MAX_VALUE);
        roomDTO.setNumber(DEFAULT_NUMBER);

        // Create the Room
        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

    }


    @Test
    public void createRoomWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = roomRepository.findAll().size();

        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setDayPeriodPrice(DEFAULT_DAY_PERIOD_PRICE);
        roomDTO.setHotelId(100L);
        roomDTO.setNumber(DEFAULT_NUMBER);
        roomDTO.setId(100L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Room in the database
        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = roomRepository.findAll().size();
        // set the field null
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setDayPeriodPrice(DEFAULT_DAY_PERIOD_PRICE);
        roomDTO.setHotelId(100L);
        roomDTO.setNumber(null);

        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkDayPeriodPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = roomRepository.findAll().size();
        // set the field null
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setDayPeriodPrice(null);
        roomDTO.setHotelId(100L);
        roomDTO.setNumber(DEFAULT_NUMBER);

        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getNonExistingRoom() throws Exception {
        // Get the room
        restRoomMockMvc.perform(get("/api/rooms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Room.class);
        Room room1 = new Room();
        room1.setId(1L);
        Room room2 = new Room();
        room2.setId(room1.getId());
        assertThat(room1).isEqualTo(room2);
        room2.setId(2L);
        assertThat(room1).isNotEqualTo(room2);
        room1.setId(null);
        assertThat(room1).isNotEqualTo(room2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoomDTO.class);
        RoomDTO roomDTO1 = new RoomDTO();
        roomDTO1.setId(1L);
        RoomDTO roomDTO2 = new RoomDTO();
        assertThat(roomDTO1).isNotEqualTo(roomDTO2);
        roomDTO2.setId(roomDTO1.getId());
        assertThat(roomDTO1).isEqualTo(roomDTO2);
        roomDTO2.setId(2L);
        assertThat(roomDTO1).isNotEqualTo(roomDTO2);
        roomDTO1.setId(null);
        assertThat(roomDTO1).isNotEqualTo(roomDTO2);
    }

    @Test
    public void testEntityFromId() {
        assertThat(roomMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(roomMapper.fromId(null)).isNull();
    }

    @Test
    public void searchForAvailableRooms() throws Exception {

        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setId(1L);
        reservationDTO.setDayPrice(150D);
        reservationDTO.setStart(LocalDate.of(2016,01,01));
        reservationDTO.setEnd(LocalDate.of(2019,10,01));
        reservationDTO.setHotelCity("Warsaw");
        reservationDTO.setRoomId(100L);

        reservationSearchRepository.save(reservationDTO);

        restRoomMockMvc.perform(get("/api/_search/rooms?startDate=2017-01-02&endDate=2018-01-02&priceMin=100&priceMax=500&city=Warsaw"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)));
    }
}
