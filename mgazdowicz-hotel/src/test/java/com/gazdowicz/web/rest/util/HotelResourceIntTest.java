package com.gazdowicz.web.rest.util;

import com.gazdowicz.HotelApp;

import com.gazdowicz.domain.Hotel;
import com.gazdowicz.repository.HotelRepository;
import com.gazdowicz.service.HotelService;
import com.gazdowicz.service.dto.HotelDTO;
import com.gazdowicz.service.mapper.HotelMapper;
import com.gazdowicz.web.DataInitializerBase;
import com.gazdowicz.web.TestUtil;
import com.gazdowicz.web.rest.HotelResource;
import com.gazdowicz.web.rest.errors.ExceptionTranslator;

import liquibase.exception.LiquibaseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HotelResource REST controller.
 *
 * @see HotelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HotelApp.class)
public class HotelResourceIntTest extends DataInitializerBase {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String DEFAULT_CITY = "AAAAAAAAAA";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private HotelMapper hotelMapper;

    @Autowired
    private HotelService hotelService;

    private MockMvc restHotelMockMvc;

    @Before
    public void initTest() throws SQLException, LiquibaseException {
        super.setupTest("hotel_data_mock.xml",jdbcTemplate);
        MockitoAnnotations.initMocks(this);
        final HotelResource hotelResource = new HotelResource(hotelService);
        this.restHotelMockMvc = MockMvcBuilders.standaloneSetup(hotelResource)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();

    }

    @After
    public void afterTest() throws LiquibaseException {
       super.afterTest();
    }


    @Test
    @Transactional
    public void createHotel() throws Exception {
        int databaseSizeBeforeCreate = hotelRepository.findAll().size();

        HotelDTO hotelDTO = new HotelDTO();
        hotelDTO.setCity(DEFAULT_CITY);
        hotelDTO.setName(DEFAULT_NAME);

        // Create the Hotel
        restHotelMockMvc.perform(post("/api/hotels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hotelDTO)))
            .andExpect(status().isCreated());

        // Validate the Hotel in the database
        List<Hotel> hotelList = hotelRepository.findAll();
        assertThat(hotelList).hasSize(databaseSizeBeforeCreate + 1);
        Hotel testHotel = hotelList.get(hotelList.size() - 1);
        assertThat(testHotel.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHotel.getCity()).isEqualTo(DEFAULT_CITY);

    }

    @Test
    @Transactional
    public void createHotelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = hotelRepository.findAll().size();

        // Create the Hotel with an existing ID
        HotelDTO hotelDTO = new HotelDTO();
        hotelDTO.setCity(DEFAULT_CITY);
        hotelDTO.setName(DEFAULT_NAME);
        hotelDTO.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHotelMockMvc.perform(post("/api/hotels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hotelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Hotel in the database
        List<Hotel> hotelList = hotelRepository.findAll();
        assertThat(hotelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = hotelRepository.findAll().size();
        // set the field null
        HotelDTO hotelDTO = new HotelDTO();
        hotelDTO.setCity(DEFAULT_CITY);
        hotelDTO.setName(null);

        restHotelMockMvc.perform(post("/api/hotels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hotelDTO)))
            .andExpect(status().isBadRequest());

        List<Hotel> hotelList = hotelRepository.findAll();
        assertThat(hotelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCityIsRequired() throws Exception {
        int databaseSizeBeforeTest = hotelRepository.findAll().size();
        // set the field null
        HotelDTO hotelDTO = new HotelDTO();
        hotelDTO.setCity(null);
        hotelDTO.setName(DEFAULT_NAME);

        restHotelMockMvc.perform(post("/api/hotels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hotelDTO)))
            .andExpect(status().isBadRequest());

        List<Hotel> hotelList = hotelRepository.findAll();
        assertThat(hotelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Hotel.class);
        Hotel hotel1 = new Hotel();
        hotel1.setId(1L);
        Hotel hotel2 = new Hotel();
        hotel2.setId(hotel1.getId());
        assertThat(hotel1).isEqualTo(hotel2);
        hotel2.setId(2L);
        assertThat(hotel1).isNotEqualTo(hotel2);
        hotel1.setId(null);
        assertThat(hotel1).isNotEqualTo(hotel2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HotelDTO.class);
        HotelDTO hotelDTO1 = new HotelDTO();
        hotelDTO1.setId(1L);
        HotelDTO hotelDTO2 = new HotelDTO();
        assertThat(hotelDTO1).isNotEqualTo(hotelDTO2);
        hotelDTO2.setId(hotelDTO1.getId());
        assertThat(hotelDTO1).isEqualTo(hotelDTO2);
        hotelDTO2.setId(2L);
        assertThat(hotelDTO1).isNotEqualTo(hotelDTO2);
        hotelDTO1.setId(null);
        assertThat(hotelDTO1).isNotEqualTo(hotelDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(hotelMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(hotelMapper.fromId(null)).isNull();
    }
}
