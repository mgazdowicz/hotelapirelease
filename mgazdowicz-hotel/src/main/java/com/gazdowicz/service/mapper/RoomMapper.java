package com.gazdowicz.service.mapper;

import com.gazdowicz.domain.*;
import com.gazdowicz.service.dto.RoomDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Room and its DTO RoomDTO.
 */
@Mapper(componentModel = "spring", uses = {HotelMapper.class, })
public interface RoomMapper extends EntityMapper <RoomDTO, Room> {

    @Mapping(source = "hotel.id", target = "hotelId")
    RoomDTO toDto(Room room); 

    @Mapping(source = "hotelId", target = "hotel")
    @Mapping(target = "reservations", ignore = true)
    Room toEntity(RoomDTO roomDTO); 
    default Room fromId(Long id) {
        if (id == null) {
            return null;
        }
        Room room = new Room();
        room.setId(id);
        return room;
    }
}
