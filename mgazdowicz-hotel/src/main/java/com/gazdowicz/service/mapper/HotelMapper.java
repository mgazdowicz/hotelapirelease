package com.gazdowicz.service.mapper;

import com.gazdowicz.domain.*;
import com.gazdowicz.service.dto.HotelDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Hotel and its DTO HotelDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HotelMapper extends EntityMapper <HotelDTO, Hotel> {
    
    @Mapping(target = "rooms", ignore = true)
    Hotel toEntity(HotelDTO hotelDTO); 
    default Hotel fromId(Long id) {
        if (id == null) {
            return null;
        }
        Hotel hotel = new Hotel();
        hotel.setId(id);
        return hotel;
    }
}
