package com.gazdowicz.service;

import com.gazdowicz.service.dto.HotelDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Hotel.
 */
public interface HotelService {

    /**
     * register a hotel.
     *
     * @param hotelDTO the entity to registerHotel
     * @return the persisted entity
     */
    HotelDTO registerHotel(HotelDTO hotelDTO);

}
