package com.gazdowicz.service;

import com.gazdowicz.service.dto.CustomerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Customer.
 */
public interface CustomerService {

    /**
     * Save a customer.
     *
     * @param customerDTO the entity to registerHotel
     * @return the persisted entity
     */
    CustomerDTO register(CustomerDTO customerDTO);

}
