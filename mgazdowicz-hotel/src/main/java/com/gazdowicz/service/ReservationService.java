package com.gazdowicz.service;

import com.gazdowicz.service.dto.ReservationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Reservation.
 */
public interface ReservationService {

    /**
     * Save a reservation.
     *
     * @param reservationDTO the entity to registerHotel
     * @return the persisted entity
     */
    ReservationDTO doReservation(ReservationDTO reservationDTO);

}
