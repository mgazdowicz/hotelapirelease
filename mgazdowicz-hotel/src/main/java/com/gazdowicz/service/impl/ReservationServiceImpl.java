package com.gazdowicz.service.impl;

import com.gazdowicz.domain.Hotel;
import com.gazdowicz.domain.Reservation;
import com.gazdowicz.domain.Room;
import com.gazdowicz.repository.HotelRepository;
import com.gazdowicz.repository.ReservationRepository;
import com.gazdowicz.repository.RoomRepository;
import com.gazdowicz.repository.search.ReservationSearchRepository;
import com.gazdowicz.service.ReservationService;
import com.gazdowicz.service.RoomService;
import com.gazdowicz.service.dto.ReservationDTO;
import com.gazdowicz.service.mapper.ReservationMapper;
import com.gazdowicz.service.mapper.RoomMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Service Implementation for managing Reservation.
 */
@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    private final Logger log = LoggerFactory.getLogger(ReservationServiceImpl.class);

    private final ReservationRepository reservationRepository;

    private final ReservationMapper reservationMapper;

    private final ReservationSearchRepository reservationSearchRepository;

    private final RoomRepository roomRepository;

    private final HotelRepository hotelRepository;

    private final RoomService roomService;

    private final RoomMapper roomMapper;

    public ReservationServiceImpl(ReservationRepository reservationRepository, ReservationMapper reservationMapper, ReservationSearchRepository reservationSearchRepository, RoomRepository roomRepository, HotelRepository hotelRepository, RoomService roomService, RoomMapper roomMapper) {
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
        this.reservationSearchRepository = reservationSearchRepository;
        this.roomRepository = roomRepository;
        this.hotelRepository = hotelRepository;
        this.roomService = roomService;
        this.roomMapper = roomMapper;
    }

    /**
     * Save a reservation.
     *
     * @param reservationDTO the entity to registerHotel
     * @return the persisted entity
     */
    @Override
    public ReservationDTO doReservation(ReservationDTO reservationDTO) {
        if (isAvailableForReservation(reservationDTO.getRoomId(), reservationDTO.getStart(), reservationDTO.getEnd())) {
            log.debug("Request to registerHotel Reservation : {}", reservationDTO);
            Reservation reservation = reservationMapper.toEntity(reservationDTO);
            reservation = reservationRepository.saveAndFlush(reservation);
            ReservationDTO result = reservationMapper.toDto(reservation);
            Room room = roomRepository.findOne(result.getRoomId());
            Hotel hotel = hotelRepository.findOne(room.getHotel().getId());
            result.setDayPrice(room.getDayPeriodPrice());
            result.setHotelCity(hotel.getCity());
            reservationSearchRepository.save(result);
            return result;
        } else {
            return null;
        }
    }

    private Boolean isAvailableForReservation(Long roomId, LocalDate start, LocalDate end) {
        Predicate<ReservationDTO> dateRangePredicate = ((Predicate<ReservationDTO>) reservationDTO ->
            reservationDTO.getStart().toEpochDay() < LocalDate.parse(start.toString()).toEpochDay())
            .or(reservationDTO -> reservationDTO.getEnd().toEpochDay() > LocalDate.parse(end.toString()).toEpochDay());
        Optional<ReservationDTO> first = reservationMapper.toDto(reservationRepository
            .findAllByRoomId(roomId)).stream().filter(dateRangePredicate).findFirst();
        return !first.isPresent();
    }


}
