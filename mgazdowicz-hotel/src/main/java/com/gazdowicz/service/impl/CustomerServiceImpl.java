package com.gazdowicz.service.impl;

import com.gazdowicz.service.CustomerService;
import com.gazdowicz.domain.Customer;
import com.gazdowicz.repository.CustomerRepository;
import com.gazdowicz.repository.search.CustomerSearchRepository;
import com.gazdowicz.service.dto.CustomerDTO;
import com.gazdowicz.service.mapper.CustomerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Customer.
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService{

    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    private final CustomerSearchRepository customerSearchRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper, CustomerSearchRepository customerSearchRepository) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
        this.customerSearchRepository = customerSearchRepository;
    }

    /**
     * Register a customer.
     *
     * @param customerDTO the entity to registerHotel
     * @return the persisted entity
     */
    @Override
    public CustomerDTO register(CustomerDTO customerDTO) {
        log.debug("Request to registerHotel Customer : {}", customerDTO);
        Customer customer = customerMapper.toEntity(customerDTO);
        customer = customerRepository.saveAndFlush(customer);
        CustomerDTO result = customerMapper.toDto(customer);
        customerSearchRepository.save(customer);
        return result;
    }

}
