package com.gazdowicz.service.impl;

import com.gazdowicz.service.HotelService;
import com.gazdowicz.domain.Hotel;
import com.gazdowicz.repository.HotelRepository;
import com.gazdowicz.repository.search.HotelSearchRepository;
import com.gazdowicz.service.dto.HotelDTO;
import com.gazdowicz.service.mapper.HotelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Hotel.
 */
@Service
public class HotelServiceImpl implements HotelService{

    private final Logger log = LoggerFactory.getLogger(HotelServiceImpl.class);

    private final HotelRepository hotelRepository;

    private final HotelMapper hotelMapper;

    private final HotelSearchRepository hotelSearchRepository;

    public HotelServiceImpl(HotelRepository hotelRepository, HotelMapper hotelMapper, HotelSearchRepository hotelSearchRepository) {
        this.hotelRepository = hotelRepository;
        this.hotelMapper = hotelMapper;
        this.hotelSearchRepository = hotelSearchRepository;
    }

    /**
     * Register a hotel.
     *
     * @param hotelDTO the entity to registerHotel
     * @return the persisted entity
     */
    @Override
    @Transactional
    public HotelDTO registerHotel(HotelDTO hotelDTO) {
        log.debug("Request to registerHotel Hotel : {}", hotelDTO);
        Hotel hotel = hotelMapper.toEntity(hotelDTO);
        hotel = hotelRepository.saveAndFlush(hotel);
        HotelDTO result = hotelMapper.toDto(hotel);
        hotelSearchRepository.save(hotel);
        return result;
    }

}
