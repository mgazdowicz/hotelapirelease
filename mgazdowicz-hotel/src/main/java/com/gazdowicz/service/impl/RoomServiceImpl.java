package com.gazdowicz.service.impl;

import com.gazdowicz.domain.Room;
import com.gazdowicz.repository.ReservationRepository;
import com.gazdowicz.repository.RoomRepository;
import com.gazdowicz.repository.search.ReservationSearchRepository;
import com.gazdowicz.repository.search.RoomSearchRepository;
import com.gazdowicz.service.RoomService;
import com.gazdowicz.service.dto.ReservationDTO;
import com.gazdowicz.service.dto.RoomDTO;
import com.gazdowicz.service.mapper.ReservationMapper;
import com.gazdowicz.service.mapper.RoomMapper;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Room.
 */
@Service
@Transactional
public class RoomServiceImpl implements RoomService {

    private final Logger log = LoggerFactory.getLogger(RoomServiceImpl.class);

    private final RoomRepository roomRepository;

    private final RoomMapper roomMapper;

    private final ReservationRepository reservationRepository;

    private final ReservationMapper reservationMapper;

    private final RoomSearchRepository roomSearchRepository;

    private final ReservationSearchRepository reservationSearchRepository;

    private final ElasticsearchTemplate elasticsearchTemplate;

    public RoomServiceImpl(RoomRepository roomRepository,
                           RoomMapper roomMapper,
                           ReservationRepository reservationRepository,
                           ReservationMapper reservationMapper, RoomSearchRepository roomSearchRepository,
                           ReservationSearchRepository reservationSearchRepository, ElasticsearchTemplate elasticsearchTemplate) {
        this.roomRepository = roomRepository;
        this.roomMapper = roomMapper;
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
        this.roomSearchRepository = roomSearchRepository;
        this.reservationSearchRepository = reservationSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    /**
     * Save a room.
     *
     * @param roomDTO the entity to registerHotel
     * @return the persisted entity
     */
    @Override
    public RoomDTO register(RoomDTO roomDTO) {
        log.debug("Request to registerHotel Room : {}", roomDTO);
        Room room = roomMapper.toEntity(roomDTO);
        room = roomRepository.saveAndFlush(room);
        RoomDTO result = roomMapper.toDto(room);
        roomSearchRepository.save(room);
        return result;
    }


    @Override
    @Transactional(readOnly = true)
    public List<RoomDTO> search(String startDateString, String endDateString, String priceMin, String priceMax, String city) {
        log.debug("Request to search for a page of Reservations for query startDate:{}," +
            "endDate:{},priceMin:{},priceMax:{},city:{}", startDateString, endDateString, priceMax, priceMax, city);

        MatchQueryBuilder cityCondition = matchQuery("hotelCity", city);
        RangeQueryBuilder priceCondition = rangeQuery("dayPrice").from(priceMin).to(priceMax).includeLower(true).includeUpper(true);

        Predicate<ReservationDTO> dateRangePredicate = ((Predicate<ReservationDTO>) reservationDTO ->
            reservationDTO.getStart().toEpochDay() < LocalDate.parse(endDateString).toEpochDay())
            .or(reservationDTO -> reservationDTO.getEnd().toEpochDay() > LocalDate.parse(startDateString).toEpochDay());

        QueryBuilder qb = QueryBuilders
            .boolQuery()
            .must(cityCondition)
            .must(priceCondition);


        List<Long> bookedRooms = elasticsearchTemplate.queryForPage(new NativeSearchQueryBuilder()
            .withQuery(matchAllQuery())
            .withQuery(qb)
            .build(), ReservationDTO.class).getContent().stream()
            .filter(dateRangePredicate)
            .map(ReservationDTO::getRoomId)
            .collect(Collectors.toList());

        return StreamSupport
            .stream(roomRepository.findAll().spliterator(), false)
            .filter(room -> !bookedRooms.contains(room.getId()))
            .map(roomMapper::toDto)
            .collect(Collectors.toList());
    }

}
