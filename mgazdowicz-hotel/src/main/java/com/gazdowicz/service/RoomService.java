package com.gazdowicz.service;

import com.gazdowicz.service.dto.RoomDTO;

import java.util.List;

/**
 * Service Interface for managing Room.
 */
public interface RoomService {

    /**
     * Save a room.
     *
     * @param roomDTO the entity to registerHotel
     * @return the persisted entity
     */
    RoomDTO register(RoomDTO roomDTO);

    /**
     * Search for the reservation corresponding to params.
     * @param startDate
     * @param endDate
     * @param priceMin
     * @param priceMax
     * @param query
     * @return
     */
    List<RoomDTO> search(String startDate, String endDate, String priceMin,String priceMax, String query);

}
