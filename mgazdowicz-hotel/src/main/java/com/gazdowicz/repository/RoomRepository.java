package com.gazdowicz.repository;

import com.gazdowicz.domain.Room;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Room entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    List<Room> findAllByNumber(Long roomId);
}
