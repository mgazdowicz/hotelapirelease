package com.gazdowicz.repository.search;

import com.gazdowicz.domain.Hotel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Hotel entity.
 */
public interface HotelSearchRepository extends ElasticsearchRepository<Hotel, Long> {
}
