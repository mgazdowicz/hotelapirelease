package com.gazdowicz.repository.search;

import com.gazdowicz.domain.Reservation;
import com.gazdowicz.service.dto.ReservationDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Reservation entity.
 */
public interface ReservationSearchRepository extends ElasticsearchRepository<ReservationDTO, Long> {
}
