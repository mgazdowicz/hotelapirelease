package com.gazdowicz.repository;

import com.gazdowicz.domain.Reservation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;


/**
 * Spring Data JPA repository for the Reservation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query("Select r from Reservation  r " +
        "where " +
        "(r.start<=:startDate and r.end>=:startDate) " +
        "OR " +
        "(r.start<=:endDate and r.end>=:endDate) " +
        "OR " +
        "(r.start<=:startDate and r.end<=:endDate) " +
        "OR " +
        "(r.start>=:startDate and r.end>=:endDate) " +
        "AND r.room.id=:id")
    Reservation roomReservationsForPeriod(Long roomId, LocalDate startDate, LocalDate endDate);

    List<Reservation> findAllByRoomId(Long roomId);
}
