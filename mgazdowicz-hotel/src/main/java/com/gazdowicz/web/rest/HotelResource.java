package com.gazdowicz.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gazdowicz.service.HotelService;
import com.gazdowicz.service.dto.HotelDTO;
import com.gazdowicz.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * REST controller for managing Hotel.
 */
@RestController
@RequestMapping("/api")
public class HotelResource {

    private final Logger log = LoggerFactory.getLogger(HotelResource.class);

    private static final String ENTITY_NAME = "hotel";

    private final HotelService hotelService;

    public HotelResource(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    /**
     * POST  /hotels : Create a new hotel.
     *
     * @param hotelDTO the hotelDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new hotelDTO, or with status 400 (Bad Request) if the hotel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/hotels")
    @Timed
    public ResponseEntity<HotelDTO> registerHotel(@Valid @RequestBody HotelDTO hotelDTO) throws URISyntaxException {
        log.debug("REST request to registerHotel Hotel : {}", hotelDTO);
        if (hotelDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new hotel cannot already have an ID")).body(null);
        }
        HotelDTO result = hotelService.registerHotel(hotelDTO);
        return ResponseEntity.created(new URI("/api/hotels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
}
