package com.gazdowicz.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gazdowicz.service.RoomService;
import com.gazdowicz.service.dto.RoomDTO;
import com.gazdowicz.web.rest.util.HeaderUtil;
import com.gazdowicz.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

/**
 * REST controller for managing Room.
 */
@RestController
@RequestMapping("/api")
public class RoomResource {

    private final Logger log = LoggerFactory.getLogger(RoomResource.class);

    private static final String ENTITY_NAME = "room";

    private final RoomService roomService;

    public RoomResource(RoomService roomService) {
        this.roomService = roomService;
    }

    /**
     * POST  /rooms : Create a new room.
     *
     * @param roomDTO the roomDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new roomDTO, or with status 400 (Bad Request) if the room has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rooms")
    @Timed
    public ResponseEntity<RoomDTO> register(@Valid @RequestBody RoomDTO roomDTO) throws URISyntaxException {
        log.debug("REST request to registerHotel Room : {}", roomDTO);
        if (roomDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new room cannot already have an ID")).body(null);
        }
        RoomDTO result = roomService.register(roomDTO);
        return ResponseEntity.created(new URI("/api/rooms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * SEARCH  /_search/rooms?query=:query : search for the room corresponding
     * to the query.
     *
     * @param query the query of the room search
     * @return the result of the search
     */
    @GetMapping("/_search/rooms")
    @Timed
    public ResponseEntity<List<RoomDTO>> search(@RequestParam String startDate,
                                                @RequestParam String endDate,
                                                @RequestParam String priceMin,
                                                @RequestParam String priceMax,
                                                @RequestParam String city) {
        log.debug("REST request to search for a page of Rooms for query {}", startDate);
        List<RoomDTO> page = roomService.search(startDate,endDate,priceMin,priceMax,city);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

}
