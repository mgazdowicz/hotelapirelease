package com.gazdowicz.web.rest.errors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.DefaultProblem;
import org.zalando.problem.Problem;
import org.zalando.problem.ProblemBuilder;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.HttpStatusAdapter;
import org.zalando.problem.spring.web.advice.ProblemHandling;
import org.zalando.problem.spring.web.advice.validation.ConstraintViolationProblem;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

import static org.zalando.problem.Problem.DEFAULT_TYPE;

@ControllerAdvice
public class ExceptionTranslator implements ProblemHandling {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_CONSTRAINT_VIOLATION = "error.constraintViolationError";
    private static final Object ERR_VALIDATION = "Error validaton";


    @Override
    public ResponseEntity<Problem> process(@Nullable ResponseEntity<Problem> entity) {
        if (entity == null || entity.getBody() == null) {
            return entity;
        }
        Problem problem = entity.getBody();
        if (!(problem instanceof ConstraintViolationProblem || problem instanceof DefaultProblem)) {
            return entity;
        }
        ProblemBuilder builder = Problem.builder()
            .withType(DEFAULT_TYPE.equals(problem.getType()) ? DEFAULT_TYPE : problem.getType())
            .withStatus(problem.getStatus())
            .withTitle(problem.getTitle());

        if (problem instanceof ConstraintViolationProblem) {
            builder.with("violations", ((ConstraintViolationProblem) problem).getViolations())
                .with("message", ERR_VALIDATION);
            return new ResponseEntity<>(builder.build(), entity.getHeaders(), entity.getStatusCode());
        } else {
            builder.withCause(((DefaultProblem) problem).getCause())
                .withDetail(problem.getDetail())
                .withInstance(problem.getInstance());
            problem.getParameters().forEach(builder::with);
            if (!problem.getParameters().containsKey("message") && problem.getStatus() != null) {
                builder.with("message", "error.http." + problem.getStatus().getStatusCode());
            }
            return new ResponseEntity<>(builder.build(), entity.getHeaders(), entity.getStatusCode());
        }
    }

    @Override
    public ResponseEntity<Problem> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, @Nonnull NativeWebRequest request) {
        BindingResult result = ex.getBindingResult();
        List<FieldErrorVM> fieldErrors = result.getFieldErrors().stream()
            .map(f -> new FieldErrorVM(f.getObjectName(), f.getField(), f.getCode()))
            .collect(Collectors.toList());

        Problem problem = Problem.builder()
            .withTitle("Method argument not valid")
            .withStatus(defaultConstraintViolationStatus())
            .with("message", ERR_VALIDATION)
            .with("fieldErrors", fieldErrors)
            .build();
        return create(ex, problem, request);
    }

    /**
     * Override default handler to take ResponseStatus annotation into account
     */
    @Override
    public ResponseEntity<Problem> handleThrowable(
        @Nonnull final Throwable throwable,
        @Nonnull final NativeWebRequest request) {
        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(throwable.getClass(), ResponseStatus.class);
        if (responseStatus != null) {
            Problem problem = Problem.builder()
                .withStatus(new HttpStatusAdapter(responseStatus.value()))
                .withTitle(responseStatus.reason().isEmpty() ? responseStatus.value().getReasonPhrase() : responseStatus.reason())
                .withDetail(throwable.getMessage())
                .build();
            return create(throwable, problem, request);
        } else {
            return create(Status.INTERNAL_SERVER_ERROR, throwable, request);
        }
    }


    @ExceptionHandler(value = {ConstraintViolationException.class, org.springframework.dao.DataIntegrityViolationException.class,org.hibernate.exception.ConstraintViolationException.class})
    public ResponseEntity<Problem> handleEntityConstraintValidationException(RuntimeException ex, NativeWebRequest request) {
        Problem problem = buildResponse(Status.BAD_REQUEST, ERR_CONSTRAINT_VIOLATION);
        return create(ex, problem, request);
    }

    protected Problem buildResponse(Status status, String message) {
        return Problem.builder()
            .withStatus(status)
            .with("message", message)
            .build();
    }
}
