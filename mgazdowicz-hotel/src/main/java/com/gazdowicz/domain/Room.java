package com.gazdowicz.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Room.
 */
@Entity
@Table(name = "room")
@Document(indexName = "room")
public class Room implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator",sequenceName = "seq_id1")
    private Long id;

    @NotNull
    @Column(name = "jhi_number", nullable = false)
    private Long number;

    @NotNull
    @Column(name = "day_period_price", nullable = false)
    private Double dayPeriodPrice;

    @ManyToOne
    private Hotel hotel;

    @OneToMany(mappedBy = "room")
    @JsonIgnore
    private Set<Reservation> reservations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public Room number(Long number) {
        this.number = number;
        return this;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Double getDayPeriodPrice() {
        return dayPeriodPrice;
    }

    public Room dayPeriodPrice(Double dayPeriodPrice) {
        this.dayPeriodPrice = dayPeriodPrice;
        return this;
    }

    public void setDayPeriodPrice(Double dayPeriodPrice) {
        this.dayPeriodPrice = dayPeriodPrice;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public Room hotel(Hotel hotel) {
        this.hotel = hotel;
        return this;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public Room reservations(Set<Reservation> reservations) {
        this.reservations = reservations;
        return this;
    }

    public Room addReservation(Reservation reservation) {
        this.reservations.add(reservation);
        reservation.setRoom(this);
        return this;
    }

    public Room removeReservation(Reservation reservation) {
        this.reservations.remove(reservation);
        reservation.setRoom(null);
        return this;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Room room = (Room) o;
        if (room.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), room.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Room{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", dayPeriodPrice='" + getDayPeriodPrice() + "'" +
            "}";
    }
}
